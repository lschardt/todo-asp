# TODO: Projekt

Datenbank löschen:


 - Developer-Eingabeaufforderung für VS2015 starten
 - sqllocaldb stop
 - sqllocaldb d
 - asp-TODO_Project...log.ldf löschen (im Projektordner)
 - asp-TODO_Project...mdf löschen (im Projektordner)

Was soll das Projekt erreichen?

TODO: Project soll das Planen von Projekten erheblich zu vereinfachen. 
Mit TODO: Project können kleine Teams schnell und ohne viel Registrierungsaufwand eine saubere Projektplanung durchführen. 

Grundkonzept:
Das Grundkonzept ist es, einfach und schnell ein kleines Projekt aufzusetzen, welches genauso schnell wieder fertig sein kann. 
Damit es keinen großen Overhead bei der Planung gibt, ist TODO: Project so konzpiert, dass Projekte ohne Registrierung/Anmeldung erstellt/geteilt und bearbeiten werden können.

Funktionen:

 - Projekt erstellen/löschen
 - Projekt teilen
 - An Projekt teilnehmen

Ein Projekt enthält:

 - Tasks
 - Teilnehmer

Ein Task enthält:

 - Autor
 - Bearbeiter
 - Label
 - Farbe
 - Status
 - Datum
 - Deadline
 - Kommentare
 - Beschreibung

Ein Teilnehmer enthält:

 - Rolle
 - Name

