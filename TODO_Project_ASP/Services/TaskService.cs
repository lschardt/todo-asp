﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TODO_Project_ASP.Models;

namespace TODO_Project_ASP.Services
{
    public class TaskService
    {
        // TODO: implement

        public TaskModel UpdateTask(TaskModel taskModel)
        {
            using (var context = new ApplicationDbContext())
            {
                context.SaveChanges();
            }

            return taskModel;
        }

        public TaskModel CreateTask(ProjectModel project, UserModel author)
        {
            var task = new TaskModel
            {
                AssigneeID = author.ID,
                AuthorID = author.ID,
                ProjectID = project.ID,
                State = TaskState.InProgress,
                DueTo = DateTime.Now,
                CreatedAt = DateTime.Now,
                Name = "Projekt registrieren", 
                Description = "A really insightful description"
            };


            using (var context = new ApplicationDbContext())
            {
                context.TaskModels.Add(task);
                context.SaveChanges();
            }

            project.Tasks.Add(task);

            return task;
        }

        private static TaskService instance = null;
        public static TaskService getInstance()
        {
            return instance ?? (instance = new TaskService());
        }
    }
}