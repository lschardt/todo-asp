﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using TODO_Project_ASP.Models;

namespace TODO_Project_ASP.Services
{
    public class UserService
    {
        public UserModel CurrentUser
        {
            get { return (UserModel)System.Web.HttpContext.Current.Session["current-user"]; }
            set { System.Web.HttpContext.Current.Session["current-user"] = value; }
        }

        public string HashPin(string pin)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] textToHash = Encoding.Default.GetBytes(pin);
            byte[] result = md5.ComputeHash(textToHash);

            return System.BitConverter.ToString(result);
        }

        public UserModel CreateForProject(ProjectModel project)
        {
            UserModel user = new UserModel();
            user.ProjectID = project.ID;
            
            return user;
        }

        public UserModel CreateForProject(ProjectModel project, string name, string pin)
        {
            UserModel user = new UserModel();

            user.HashedPin = HashPin(pin);
            user.ProjectID = project.ID;
            user.Name = name;

            return user;
        }

        private static UserService instance = null;
        public static UserService getInstance()
        {
            return instance ?? (instance = new UserService());
        }
    }
}