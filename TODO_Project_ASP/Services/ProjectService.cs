﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using TODO_Project_ASP.Models;

namespace TODO_Project_ASP.Services
{
    public class ProjectService
    {
        public ProjectModel CurrentProject
        {
            get { return (ProjectModel)System.Web.HttpContext.Current.Session["current-project"]; }
            set { System.Web.HttpContext.Current.Session["current-project"] = value; }
        }

        /**
         * Should create a new unique link for a project.
         */
        public string CreateUniqueProjectLink(ProjectModel model)
        {
            return "http://localhost:50931/project/overview?Guid=" + model.Guid;
        }

        public string CreateUniqueProjectJoinLink(ProjectModel model)
        {
            return "http://localhost:50931/project/join?projectGuid=" + model.Guid;
        }

        public string GenerateProjectGuid()
        {
            return Guid.NewGuid().ToString();
        }

        public ProjectModel BuildProject(ProjectModel rawModel)
        {

            UserService userService = UserService.getInstance();

            using (var context = new ApplicationDbContext())
            {
                // save the project without reference to the user.
                context.ProjectModels.Add(rawModel);
                context.SaveChanges();

                rawModel.Guid = this.GenerateProjectGuid();

                // create a new unqiue project link for this model.
                rawModel.ProjectLink = this.CreateUniqueProjectLink(rawModel);
                rawModel.ProjectJoinLink = this.CreateUniqueProjectJoinLink(rawModel);

                context.SaveChanges();
            }
            
            return rawModel;
        }

        public List<TaskModel> GetTaksInState(ProjectModel project, TaskState state)
        {
            return project.Tasks?.FindAll((task) => task.State == state) ?? new List<TaskModel>();
        }


        private static ProjectService instance = null;
        public static ProjectService getInstance()
        {
            return instance ?? (instance = new ProjectService());
        }
    }
}