﻿function allowDrop(ev) {
    ev.preventDefault();
}

window.tododrag = function (ev) {
    var element = jQuery(ev.target);
    window.currentId = element.data("id");
    ev.dataTransfer.setData("id", window.currentId);
};

function drop(ev,state) {
    ev.preventDefault();
    $.ajax({
        url: "http://localhost:50931/Tasks/UpdateState",
        dataType: "json",
        data: {ID:window.currentId,State:state},
        method: "POST",
        success: function (result) {
            ev.target.appendChild(document.getElementById("single-task-container-" + window.currentId));
        }
    });
}
