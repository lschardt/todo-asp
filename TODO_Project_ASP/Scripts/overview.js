﻿(function($) {

    function initializeTagSelectBox() {
        var container = $(".select-tag-container");
        var tagToggle = $("#modal-task-details-tag");

        console.log("set click listener");

        tagToggle.click(function (event) {


            $.ajax({
                url: "http://localhost:50931/Tag/All",
                dataType: "json",
                method: "GET",
                success: function (result) {
                    console.log(result);
                    var container = $(".select-tag-container");
                    for (var i = 0; i < result.length; i++) {
                        container.append("<div class='select-tag-tag' data-id='" + result[i].ID + "'>" + result[i].Name + "</div>");
                    }



                    $(".select-tag-tag:not(.create-tag)")
                         .click(function (event) {
                             console.log("yeah");

                             // close the box and set the tag accordingly.

                             container.addClass("hidden");

                             var element = $(this);
                             console.log(this);
                             console.log("id",
                                 element.data("id"));
                             console.log("current ask id", window.currentTaskId);

                             $.ajax({
                                 url: "http://localhost:50931/Tasks/UpdateTag",
                                 dataType: "json",
                                 data: { TagID: element.data("id"), TaskID: window.currentTaskId },
                                 method: "POST",
                                 success: function (result) {
                                     $("#modal-task-details-tag").text(element.text());
                                 }
                             });


                         });
                }
            });

            container.removeClass("hidden");
        });

        container.find("input")
            .keyup(function(event) {
                if (event.keyCode == 13) {
                    // enter just got pressed.

                    var element = $(this);
                    var value = element.val();
                    $.ajax({
                        url: "http://localhost:50931/Tag/Add",
                        dataType: "json",
                        data: { Name: value, TaskID: window.currentTaskId },
                        method: "POST",
                        success: function (result) {
                            console.log("added tag", result);
                            element.val("");
                            container.addClass("hidden");
                            $("#modal-task-details-tag").text(value);
                        }
                    });
                }
            });
    }

    $(document)
        .ready(function() {

            initializeTagSelectBox();

            window.showTaskDetailModal = function (taskId) {
                var modal = $("#modal-task-details");
                modal.modal("show");

                window.currentTaskId = taskId;

                $.ajax({
                    url: "http://localhost:50931/Tasks/Single/" + taskId,
                    dataType: "json",
                    method: "GET",
                    success: function (result) {
                        var description = result.Description;
                        var name = result.Name;

                        $("#modal-task-details-description").text(description);
                        $("#modal-task-details-title").text(name);
                        $("#modal-task-details-author").text(result.AuthorName);
                        $("#modal-task-details-assignee").text(result.AsigneeName);
                        $("#modal-task-details-due-to").text(result.FormattedDueTo);
                        $("#modal-task-details-tag").text(result.TagName);
                    }
                });
            };

        });


})(jQuery);
