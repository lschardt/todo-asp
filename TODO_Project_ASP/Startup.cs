﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TODO_Project_ASP.Startup))]
namespace TODO_Project_ASP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
