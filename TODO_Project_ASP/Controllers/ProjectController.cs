﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TODO_Project_ASP.Models;
using TODO_Project_ASP.Services;

namespace TODO_Project_ASP.Controllers
{
    public class ProjectController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //
        // POST: /Project/Create
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProjectCreateViewModel projectCreateModel)
        {
            if (!ModelState.IsValid)
            {
                // Redirect to index.
                Debug.WriteLine("create project - model state not valid");
                return RedirectToAction("Index", "Home");
            }
            
            /*
             * Let the service populate the model with relevant information
             * like the project link.
             */
            ProjectService service = ProjectService.getInstance();
            var projectModel = new ProjectModel()
            {
                ProjectName = projectCreateModel.ProjectName
            };
            projectModel = service.BuildProject(projectModel);


            var user = UserService.getInstance().CreateForProject(projectModel,
                projectCreateModel.UserName, projectCreateModel.UserPin);

            user.ProjectID = projectModel.ID;
            db.UserModels.Add(user);
            db.SaveChanges();

            // set the current user.
            UserService.getInstance().CurrentUser = user;

            Debug.WriteLine("project created");

            return RedirectToAction("Overview", projectModel);
        }

        [System.Web.Mvc.HttpGet]
        public ViewResult List()
        {
            DbSet models = this.db.ProjectModels;
            return View(models);
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult Overview(ProjectModel model)
        {
            Debug.Write("mode guid: ");
            Debug.WriteLine(model.Guid);

            if (model.Guid == null || model.Guid.Trim().Length == 0)
            {
                return RedirectToAction("Index", "Home");
            }

            // You need to fetch the model from the database before accessing the fields.
            // It could be that only the guid is set.
            model = db.ProjectModels.SingleOrDefault(project => project.Guid == model.Guid);
           

            if (null == model)
            {
                Debug.WriteLine("the model is null");
                // redirect to the index if there is no project with this guid.
                return RedirectToAction("Index", "Home");
            }

            if (null == UserService.getInstance().CurrentUser)
            {
                RouteValueDictionary dictionary = new RouteValueDictionary();
                dictionary.Add("projectGuid", model.Guid);
                // the user is not "logged in" redirect to the login page
                return RedirectToAction("Login", "Project", dictionary);
            }

            ProjectService.getInstance().CurrentProject = model;

            return View(model);
        }


        [System.Web.Mvc.HttpGet]
        public ViewResult Join(string projectGuid)
        {
            var projectJoinViewModel = new ProjectJoinViewModel()
            {
                ProjectGuid = projectGuid
            };

            return View(projectJoinViewModel);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Join(ProjectJoinViewModel model)
        {
            var project = this.db.ProjectModels.SingleOrDefault(projectModel => projectModel.Guid == model.ProjectGuid);
            if (project == null)
            {
                return View(model);
            }

            // fetch the user from the database or create a new one if there is no user with this id.
            var user =
                this.db.UserModels.SingleOrDefault(
                    userModel => userModel.ProjectID == project.ID && userModel.Name == model.UserName);

            if (user != null)
            {
                // the user already exists
                return RedirectToAction("Login");
            }
                
            user = UserService.getInstance().CreateForProject(project, model.UserName, model.UserPin);
            
            user.ProjectID = project.ID;
            db.UserModels.Add(user);
            db.SaveChanges();

            // set the current user.
            UserService.getInstance().CurrentUser = user;

            return RedirectToAction("Overview", project);
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult Login(string projectGuid)
        {
            var projectJoinViewModel = new ProjectJoinViewModel()
            {
                ProjectGuid = projectGuid
            };

            return View(projectJoinViewModel);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Login(ProjectJoinViewModel model)
        {
            var project = this.db.ProjectModels.SingleOrDefault(projectModel => projectModel.Guid == model.ProjectGuid);
            if (project == null)
            {
                return View(model);
            }

            // fetch the user from the database or create a new one if there is no user with this id.
            var user =
                this.db.UserModels.SingleOrDefault(
                    userModel => userModel.ProjectID == project.ID && userModel.Name == model.UserName);

            if (user == null)
            {
                // the user does not exist
                return RedirectToAction("Join");
            }

            if (user.HashedPin != UserService.getInstance().HashPin(model.UserPin))
            {
                // the pin is not correct.
                return View(model);
            }

            // set the current user.
            UserService.getInstance().CurrentUser = user;


            return RedirectToAction("Overview", project);
        }
    }
}
