﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TODO_Project_ASP.Models;
using TODO_Project_ASP.Services;

namespace TODO_Project_ASP.Controllers
{
    public class TagController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /**
         * Used to access retrieve all tags that
         * are saved in the database and are associated with a certain project
         */
        public ActionResult All()
        {
            if (ProjectService.getInstance().CurrentProject == null)
                return Json(new List<TagModel>(), JsonRequestBehavior.AllowGet);
            var projectId = ProjectService.getInstance().CurrentProject.ID;

            var context = new ApplicationDbContext();

            var tags = context.TagModels.Where(b => b.ProjectID == projectId).ToList();
            var tagResults = new List<TagModel>();

            foreach (var tag in tags)
            {
                tagResults.Add(new TagModel()
                {
                    ID = tag.ID,
                    Name = tag.Name
                });
            }
            return Json(tagResults, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Add(AddTagModel addTag)
        {
            var tag = new TagModel()
            {
                Name = addTag.Name,
                ProjectID = ProjectService.getInstance().CurrentProject.ID
            };

            this.db.TagModels.Add(tag);
            this.db.SaveChanges();

            var task = this.db.TaskModels.SingleOrDefault(dbtask => dbtask.ID == addTag.TaskID);
            task.TagID = tag.ID;

            this.db.SaveChanges();

            return Json(tag);
        }
    }
}