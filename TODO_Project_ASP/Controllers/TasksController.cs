﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TODO_Project_ASP.Models;
using TODO_Project_ASP.Services;

namespace TODO_Project_ASP.Controllers
{
    public class TasksController : Controller
    {
        // Get: /Tasks/CreateInProject
        [System.Web.Mvc.HttpGet]
        public ViewResult CreateInProject()
        {
            // wird im Overview inkludiert
            return View();
        }

        [System.Web.Mvc.HttpPost]
        // Post: /Tasks/Create
        public ActionResult Create([FromBody] TaskModel taskModel)
        {
            taskModel.ProjectID = ProjectService.getInstance().CurrentProject.ID;
            taskModel.AssigneeID = UserService.getInstance().CurrentUser.ID;
            taskModel.AuthorID = UserService.getInstance().CurrentUser.ID;

            taskModel.CreatedAt = DateTime.Now;


            using (var context = new ApplicationDbContext())
            {
                context.TaskModels.Add(taskModel);
                context.SaveChanges();

                return RedirectToAction("Overview", "Project", ProjectService.getInstance().CurrentProject);
            }
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult UpdateState(TaskModel taskModel)
        {
            var id = taskModel.ID;
            var state = taskModel.State;
            using (var context = new ApplicationDbContext())
            {
                var dbmodel = context.TaskModels.SingleOrDefault((model) => model.ID == id);
                dbmodel.State = state;
                context.SaveChanges();
            }
            return Json(taskModel);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult UpdateTag(UpdateTagModel model)
        {
            var context = new ApplicationDbContext();
           
            var task = context.TaskModels.SingleOrDefault(m => m.ID == model.TaskID);
            task.TagID = model.TagID;

            context.SaveChanges();

            return Json(true);
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult Single(TaskModel taskModel)
        {
            Debug.WriteLine("single task");
            Debug.WriteLine(taskModel.ID);
            var context = new ApplicationDbContext();

            taskModel = context.TaskModels.Find(taskModel.ID);

           
            var jsonData = new TaskDetailsViewModel()
            {
                ID = taskModel.ID,
                Name = taskModel.Name,
                Description = taskModel.Description,
                FormattedCreatedAt = taskModel.CreatedAt.ToShortDateString(),
                FormattedDueTo = taskModel.DueTo.ToShortDateString(),
                AuthorName =  taskModel.Author.Name,
                AsigneeName = taskModel.Assignee.Name,
                TagName = (taskModel.Tag == null) ? "Noch kein Tag" : taskModel.Tag.Name
               // State = taskModel.State
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } 


    }
}