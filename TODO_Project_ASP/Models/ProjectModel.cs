﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using TODO_Project_ASP.Services;

namespace TODO_Project_ASP.Models
{
    public class ProjectModel
    {
        public ProjectModel()
        {
            this.Tasks = new List<TaskModel>();
        }

        public int ID { get; set; }
        [Required]
        [Display(Name = "Projektname")]
        public string ProjectName { get; set; }
        public string ProjectLink { get; set; }
        public string ProjectJoinLink { get; set; }

        public string Guid { get; set; }

        [ScriptIgnore]
        public virtual List<TaskModel> Tasks { get; set; }

        [ScriptIgnore]
        public virtual ICollection<UserModel> Users { get; set; }

        public ProjectService GetService()
        {
            return ProjectService.getInstance();
        }
    }
}