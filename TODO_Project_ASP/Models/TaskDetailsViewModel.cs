﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TODO_Project_ASP.Models
{
    public class TaskDetailsViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FormattedDueTo { get; set; }
        public string FormattedCreatedAt { get; set; }
        public string AsigneeName { get; set; }
        public string AuthorName { get; set; }
        public string TagName { get; set; }
    }
}