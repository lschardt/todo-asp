﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TODO_Project_ASP.Models
{
    public class CreateTagModel
    {
        public int TaskID { get; set; }

        public string Color { get; set; }
        public string Name { get; set; }
    }
}