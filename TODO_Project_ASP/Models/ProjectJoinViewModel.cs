﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace TODO_Project_ASP.Models
{
    public class ProjectJoinViewModel
    {
        public string ProjectGuid { get; set; }

        public string UserName { get; set; }

        [DisplayName("Dein 4 stelliger Pin")]
        public string UserPin { get; set; }
    }
}