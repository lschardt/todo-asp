﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TODO_Project_ASP.Models
{
    public class UserModel
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string HashedPin { get; set; }
        [ScriptIgnore]
        public virtual ProjectModel Project { get; set; }
    }
}