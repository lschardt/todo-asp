﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TODO_Project_ASP.Models
{
    public class ProjectCreateViewModel
    {
        [DisplayName("Projektname")]
        public string ProjectName { get; set; }
        [DisplayName("Benutzername")]
        public string UserName { get; set; }
        [DisplayName("Dein 4 stelliger Pin")]
        public string UserPin { get; set; }
    }
}