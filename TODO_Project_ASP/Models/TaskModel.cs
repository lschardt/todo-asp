﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TODO_Project_ASP.Models
{
    public class TaskModel
    {
        public int ID { get; set; }

        [ScriptIgnore]
        public int ProjectID { get; set; }
        public int AuthorID { get; set; }
        public int AssigneeID { get; set; }
        public int? TagID { get; set; }


        [ScriptIgnore]
        public virtual ProjectModel Project { get; set; }
        [ScriptIgnore]
        public virtual UserModel Author { get; set; }
        [ScriptIgnore]
        public virtual UserModel Assignee { get; set; }
        [Display(Name = "Beschreibung")]
        public string Description { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Deadline")]
        public DateTime DueTo { get; set; }
        public virtual TagModel Tag { get; set; }
        public TaskState State { get; set; }
    }

    public class TagModel
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string Color { get; set; }
        public string Name { get; set; }

        public virtual ProjectModel Project { get; set; }
    }

    public enum TaskState
    {
        Todo = 0,
        InProgress,
        Verify,
        Done
        
    }
}